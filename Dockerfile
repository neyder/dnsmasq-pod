#Instalar

FROM registry.access.redhat.com/ubi8/ubi

RUN yum install -y dnsmasq

EXPOSE 5353/tcp
EXPOSE 5353/udp

VOLUME ["/opt/dnsmasq"]

ENTRYPOINT ["dnsmasq", "--no-daemon", "--log-queries", "--no-hosts", "--no-resolv", "--no-negcache", "--no-poll"]

CMD ["--conf-file=/opt/dnsmasq/config"]
